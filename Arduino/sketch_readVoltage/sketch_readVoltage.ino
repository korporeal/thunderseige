//#include <SerialCommand.h>

String spaceOccupants[] = {"EMPTY","EMPTY","EMPTY","EMPTY"};
byte analogPins[] = {A0,A1,A2,A3};
int spaceCount = 4;

const int sampleDelayRate = 10;
const int updateInterval = 1000;
const int failTolerance = 10;

//SerialCommand sCmd;

//--------------- INIT ------------------
void setup() {
  
  //init 
  Serial.begin(9600);
  
}


//--------------- TICK ------------------
void loop() {
  
  delay(updateInterval);

  //iterate over each space, see what's up
  checkSpaces();
 
}


void checkSpaces() {

  //Serial.println("checkSpaces()");

  //loop through each space 
  for (int i = 0;i < sizeof(analogPins); i++) {

    //int tmp = analogRead(analogPins[i]);
    //String tmpStr = "Space" + (String)i + " - "+ (String)tmp;
    //Serial.println(tmpStr);

    //check to see if the voltage is stable and consistent
    bool stablePort = voltageStable(analogPins[i]);

    //if it's a stable voltage (piece is on)
    if (stablePort == true) {

      //String outStr = "Space" + (String)i + " stable";
      //Serial.println(outStr); 

      //get voltage
      int pieceVoltage = analogRead(analogPins[i]);

      //find out which (String)name corresponds
      String pieceName = whichPiece(pieceVoltage);

      //Serial.println(pieceName);

      //if it didn't flake
      if (pieceName != "ERROR") {

        //now let's announce it
        pieceOnSpace(pieceVoltage,i,pieceName);
        
      }
      
    }//end if

  }//end for

  Serial.println("\n");
  
}//end f


String whichPiece(int val) {

  String tmp = "whichPiece():val - " + (String)val;
  Serial.println(tmp);

  if (val < 200) {
    return "Berzerker";
  }

  if (val>201 && val < 500) {
    return "Sorcerer";
  }

  if (val>501 && val < 750) {
    return "Ranger";
  }

  if (val>751 && val < 1024) {
    return "Fairy";
  }
  
  return "ERROR";//b/c how the f could you not get 0-1023?
}

void pieceOnSpace(int resistance, int space, String piece) {

  int new_space = space+1;
  
  String tmp = (String)resistance + ":Space: " + (String)new_space + " -- Player Class: " + piece + "\n";
  Serial.flush();
  Serial.println(tmp);
}

//see if the port's voltage is steady vs erratic (4 samples & avg)
bool voltageStable(byte port) {

    int s1;
    int s2;
    int s3;
    int s4;

    int failScore= 0;

    int failLimit = 2;

    s1 = analogRead(port);
    delay(sampleDelayRate);
    
    s2 = analogRead(port);
    delay(sampleDelayRate);
    
    s3 = analogRead(port);
    delay(sampleDelayRate);
    
    s4 = analogRead(port);
    delay(sampleDelayRate);

    if ((max(s1,s2) - min(s1,s2)) > failTolerance) { failScore++; }
    if ((max(s2,s3) - min(s2,s3)) > failTolerance) { failScore++; }
    if ((max(s3,s4) - min(s3,s4)) > failTolerance) { failScore++; }

    //String tmp = "failScore: " + (String)failScore;
    //Serial.println(tmp);

    if (failScore < failLimit) { 
      //String passStr = (String)port + " pass"; 
      //Serial.println(passStr);
      return true; 
    }
    //Serial.println("fail");
    return false;
}



