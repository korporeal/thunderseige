# README #

Reliable builds might be an issue until we get a few things worked out.

### What is this repository for? ###

* Unity3d, Arduino C++ Sketches, STL 3D files
* 0.0.1

### How do I get set up? ###

* Install a copy of Unity3d
* Get access to a 3D printer
* Get Arduino hardware (UNO starter kit is good for now). We'll have a full list.
* Install the Arduino IDE

### Contribution guidelines ###

* None just yet. We're not even 100% sure this stupid thing will ever work

### Who do I talk to? ###

* Repo owner or admin
