﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MiniGame : MonoBehaviour {

	/*
	 * Traps
	 * 	- quicksand: fast tap
	 * 	- swarm attack: fast tap, class tap
	 * 	- room trap: puzzle combos
	 * Treasure Hunt
	 * 	- dragon sneak
	 * 	- goblin bonk
	 * 	Battle
	 * 	- MiniBoss
	 * 	- TrapBoss
	 * 	- BIG Boss
	 */ 

	List<Player> players = new List<Player>();

	void startup(){
		//connect to Server/Client
	}
	
	public bool AddPlayer(Player tmp){
	
		if (players.Contains(tmp)) {
			return false;
		}

		players.Add(tmp);
		return true;
	
	}
	
	void BeginGame(){}

	void UpdateGame(){}

	void UpdateBroadcast(){}

	void EndGame(){}

	void SendStats(){}

}

public enum MiniGameType {
	None,
	Trap,
	TreasureHunt,
	Battle
}