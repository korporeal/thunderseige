﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Buff : MonoBehaviour {

	public BuffType type = BuffType.None;
	public int potency;
	public bool collected = false;
	public string name;

	public void randomBuffBuildPotion(bool potion) {

		string[] tmp = System.Enum.GetNames(typeof(BuffType));

		potency = Server.randomNum(0, 50);

		foreach(string i in tmp) {

			switch(i) {

				case "Health":
					name = buffNameGenerator(BuffType.Health, potion);
					type = BuffType.Health;
				break;

				case "Attack":
					name = buffNameGenerator(BuffType.Attack, potion);
					type = BuffType.Attack;
				break;

				case "Speed":
					name = buffNameGenerator(BuffType.Speed, potion);
					type = BuffType.Speed;
				break;

				case "Vision":
					name = buffNameGenerator(BuffType.Vision, potion);
					type = BuffType.Vision;
				break;

				case "Invisibility":
					name = buffNameGenerator(BuffType.Invisibility, potion);
					type = BuffType.Invisibility;
				break;

			}//end switch

		}//end foreach

	}//end f


	public static string buffNameGenerator(BuffType buff_type,bool potion) {

		string buffName = "";

		string[] potionBaseNames = {
			"Elixr",
			"Vial",
			"Flask",
			"Gourd",
			"Sheepskin",
			"Bladder"
		};


		string[] artifactBaseNames = {
			"Synthian",
			"Corthinian",
			"Diorian",
			"Cinnamonarian",
			"Krom's",
			"Khan's",
			"Vger's",
			"Gozer's",
			"StayPuft's",
			"Jobu's"
		};

		string[] artifactMiddleNames = {
			" Amulet ",
			" Skull ",
			" Vessel ",
			" Totem ",
			" Dagger ",
			" Shrunken Head ",
			" Tooth "
		};

		string[] buffTypes = {
			" of Health",
			" of Attack",
			" of Speed",
			" of Vision",
			" of Invisibility"
		};

		//if potion
		if (potion) {

			string potionBaseName = potionBaseNames[Random.Range(0, potionBaseNames.Length)];

			buffName += potionBaseName;

			switch(buff_type) {

				case BuffType.Attack:
					buffName += buffTypes[0];
				break;

				case BuffType.Health:
					buffName += buffTypes[1];
				break;

				case BuffType.Speed:
					buffName += buffTypes[2];
				break;

				case BuffType.Vision:
					buffName += buffTypes[3];
				break;

				case BuffType.Invisibility:
					buffName += buffTypes[4];
				break;

			}

			return buffName;

		}//end if potion


		//if artifact
		if(!potion) {

			int base_key = Random.Range(0, artifactBaseNames.Length);
			string artifactBaseName = artifactBaseNames[base_key];

			buffName += artifactBaseName;

			string artifactName = artifactMiddleNames[Random.Range(0,artifactMiddleNames.Length)];

			buffName += artifactName;

			switch(buff_type) {

				case BuffType.Attack:
					buffName += buffTypes[0];
				break;

				case BuffType.Health:
					buffName += buffTypes[1];
				break;

				case BuffType.Speed:
					buffName += buffTypes[2];
				break;

				case BuffType.Vision:
					buffName += buffTypes[3];
				break;

				case BuffType.Invisibility:
					buffName += buffTypes[4];
				break;

			}

			return buffName;

		}

		return null;

	}//end f


	public void useBuff(PlayerAttributes p, Buff b) {

		switch(b.type) {

			case BuffType.Health:
				p.health += b.potency;
			break;


			case BuffType.Attack:
				p.attack += b.potency;
			break;


			case BuffType.Speed:
				p.speed += b.potency;
			break;


			case BuffType.Vision:
				p.vision += b.potency;
			break;


			case BuffType.Invisibility:
				p.invisibility += b.potency;
			break;


		}//end switch
				
	}//end f




}//end c



[System.Serializable]
public enum BuffType {
	None,
	Health,
	Attack,
	Speed,
	Vision,
	Invisibility
}
