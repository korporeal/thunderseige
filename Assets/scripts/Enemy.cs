﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour {

	public string name;
	public PlayerAttributes attributes;
	public List<Player> players = null;
	public List<Shard> shards = new List<Shard>();

	public void init() {
		getPlayers();
	}

	public void getPlayers() {
		players = Server.getPlayers();
	}

	public void attack_strongestPlayer() {

		Player p = Server.player_findStrongest();
		bool stun = Server.calcChance(attributes.inflict_stun_chance, 100);

		Server.enemy_attack(AttackType.Mele, p, this);

		if(stun) {
			Server.enemy_attack(AttackType.Stun, p, this);
		}

	}//end f



}//end class

