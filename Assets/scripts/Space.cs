﻿using UnityEngine;
using System.Collections;

public class Space : MonoBehaviour {

	public int id_space;
	public GameObject id_character;

	public Vector3 player_loc;

	public bool is_empty = true;

	public bool has_player = false;
	public Player spacePlayer = null;

	public bool has_miniGame = false;
	public MiniGameType spaceMiniGameType = MiniGameType.None;
	public MiniGame spaceMiniGame = null;

	public bool has_artifact = false;
	public Artifact spaceArtifact = null;

	public bool has_powerUp = false;
	public Potion spacePotion = null;


	public void set_space_material(Material mat) {
		transform.GetComponent<Renderer>().material = mat;
	}


	//------------- PLAYER ------------------

	public void addPlayer(Player p) {
		spacePlayer = p;
		has_player = true;
	}

	public void playerAddTrigger() {

		//addon
		if(has_artifact) {

		}

	}

	public void removePlayer() {
		has_player = false;
		spacePlayer = null;
	}

	public void movePlayer(Space newSpace) {
		newSpace.addPlayer(spacePlayer);
		removePlayer();
	}



	//------------- MINIGAME ------------------

	public void addMiniGame(MiniGame g, MiniGameType t) {

		spaceMiniGame = g;
		spaceMiniGameType = t;

	}
		

	public void setMiniGame() {
		
		switch(spaceMiniGameType) {

			case MiniGameType.Battle:
				//battle triggers
				//
			break;

			case MiniGameType.Trap:
				//battle triggers
			break;

			case MiniGameType.TreasureHunt:
				//
			break;

		}
	}


	//------------- ARTIFACTS ------------------


	//------------- POTIONS ------------------
}
