﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public static class Server {

	static string debugData;
	static bool debugConsoleWrite = true;

	static int totalTurnCount = 0;

	static ArduinoLink a_link;

	//Player Data
	static List<Player> players;
	static Player currentPlayer;

	static List<Buff> powerups;


	//Enemy data
	static List<Enemy> enemies;

	//Level Data
	static List<Level> levels;
	static List<LevelData> levelData;

	static List<MiniGame> miniGames;
	static List<MiniGameData> miniGamesData;

	static List<Trap> traps;
	static List<TrapData> trapData;

	static List<Shard> allShards;
	static List<Shard> collectedShards;

	static List<TreasureHunt> treasureHunts;
	//static List<TreasureHuntData> treasureHuntData;

	static List<Battle> battles;
	//static List<BattleData> battleData;

	static GameObject cam;

	public static void init() {

		//---- init vars -----
		players = new List<Player>();
		powerups = new List<Buff>();

		//Level Data
		levels = new List<Level>();
		traps = new List<Trap>();

		allShards = new List<Shard>();
		collectedShards = new List<Shard>();

		enemies = new List<Enemy>();


		//--- register players -----


		//register levels

		//register enemies


	}


		

	public static void registerArduinoLInk(ArduinoLink a) {
		a_link = a;
	}

	public static void registerMainCam(GameObject c) {
		cam = c;
	}

	public static GameObject getMainCam() {
		return cam;
	}


	//------------ PLAYER ---------------

	public static bool registerPlayer(Player p) {
		if (players.Contains(p)){ 
			return false; 
		}

		players.Add(p);
		return true;
	}

	public static List<Player> getPlayers() {
		return players;
	}

	public static Player spawnPlayer(string name, PlayerType type, PlayerAttributes a = default(PlayerAttributes)) {

		GameObject playerSpawn = (GameObject)Object.Instantiate(Resources.Load("PLAYER_PREFAB")) as GameObject;
		Player p = playerSpawn.GetComponent<Player>();

		if(!players.Contains(p)) {
			players.Add(p);
			return p;
		}

		return null;
	}

	public static Player getPlayerByName(string id) {

		foreach(Player i in players) {
			
			if(i.name == id) {
				return i;
			}

		}
		return null;
	}


	public static void setActivePlayer(string p) {
		currentPlayer = getPlayerByName(p);
	}


	public static void player_damageHealth(Player p, Enemy e) {

		p.attributes.health -= e.attributes.attack;

	}


	public static void player_gotPoisoned(Player p, Enemy e) {

		p.attributes.poisoned = true;

	}


	public static void player_gotPlagued(Player p, Enemy e) {
		p.attributes.plagued = true;
	}

	public static void player_gotStunned(Player p, Enemy e) {
		p.attributes.stunned = true;
	}




	//------------ ENEMY ---------------

	public static bool addEnemy(Enemy e) {

		if(!enemies.Contains(e)) {
			enemies.Add(e);
			return true;
		}
		return false;
	}

	public static void enemy_attack(AttackType a,Player p,Enemy e) {

		switch(a) {

			case AttackType.Mele: 
				player_damageHealth(p, e);
			break;

			case AttackType.Plague:
				player_gotPlagued(p, e);
			break;

			case AttackType.Poison:
				player_gotPoisoned(p, e);
			break;

			case AttackType.Stun:
				player_gotStunned(p, e);
			break;
				
		}//end switch

	}//end f



	public static Player player_findMostVisible() {

		int visibility_rating = 0;
		Player visibility_player = null;

		foreach(Player i in players) {

			int new_visibility_rating = i.attributes.invisibility;

			if(new_visibility_rating > visibility_rating) {
				visibility_player = i;
				visibility_rating = new_visibility_rating;
			}

		}//end foreach

		return visibility_player;

	}//end f


	public static Player player_findHealthiest() {

		int health_rating = 0;
		Player healthy_player = null;

		foreach(Player i in players) {

			int new_health_rating = i.attributes.health;

			if(new_health_rating > health_rating) {
				healthy_player = i;
				health_rating = new_health_rating;
			}

		}//end foreach

		return healthy_player;

	}


	public static Player player_findStrongest() {

		int strength_rating = 0;
		Player strong_player = null;

		foreach(Player i in players) {

			int new_strength_rating = i.attributes.attack;

			if(new_strength_rating > strength_rating) {
				strong_player = i;
				strength_rating = new_strength_rating;
			}

		}//end foreach

		return strong_player;

	}


	public static bool checkForEnoughShards(List<Shard> shards) {

		int shardCountForPass = shards.Count;
		int currentCount = 0;

		//loop through each player
		foreach(Player i in players) {

			//loop through each player's shards
			foreach(Shard s in i.shards) {

				//match - same shard in our inventory as Player
				if(shards.Contains(s)) {
					currentCount++;
				}

			}//end foreach

		}//end foreach

		//they have them all
		if(shardCountForPass == shards.Count) {
			return true;
		}

		return false;

	}//end f


	//------------ COLLECTIBLES ---------------


	//------------ SPACE ---------------





	//------------ LEVEL ---------------

	public static bool addLevel(Level l) {
		if (levels.Contains(l)){ 
			return false; 
		}

		levels.Add(l);
		return true;
	}


	public static Level getRegisterLevel() {

		foreach(Level l in levels) {

			if(l.type == LevelType.Register) {
				return l;
			}

		}

		return null;

	}//end f


	//------------ TURNS ---------------

	public static void turnBegin(Player p) {

		//Player selected
		//Player rolls dice
		//Player moves
		// - if potion, if artifact, if minigame, if teleport

		//? Quests?
		//? Artifacts expire?
		//? Use Artifact
		//? Use Potion on another player

		//----- CHANCES ------
		//dice
		//potion,hero placement
		//	- means change strategy

	}

	public static void turn(Player p) {



	}

	public static void turnEnd(Player p) {
		Server.totalTurnCount++;
	}



	//------------ QUESTS ---------------

	public static bool doWeAllHavePortKeys() {

		foreach(Player i in players) {

			if(i.portalKey == null) {
				return false;
			}

		}
			
		return true;

	}


	//------------ TOOLS ---------------

	public static void goToScene(string s) {

		Application.LoadLevel(s);

	}

	public static void o (string str) {

		debugData += str + "\n";

		if (debugConsoleWrite) {
			Debug.Log(str + "\n");
		}

	}

	public static int randomNum(int low, int high) {

		return Random.Range(low, high);

	}

	public static bool calcChance(int percent_chance, int chances) {

		Random rand = new Random(); 

		int yes = 0;

		for (int i = 0; i < chances; i++){

			if (randomNum(1, 101) <= percent_chance){
				yes++;
			}

		}//end for

		if(yes > percent_chance) {
			return true;
		}

		return false;

	}//end f

	public static Vector3 v3scaler(Vector3 v, float s) {

		float tmp_x = v.x * s + v.x;
		float tmp_y = v.y * s + v.y;
		float tmp_z = v.z * s + v.z;

		return new Vector3(tmp_x, tmp_y, tmp_z);


	}


}//end class


[System.Serializable]
public class LevelData {
	public Level level_obj;
	public List<Space> spaces;
	public MiniGameType type;
	public MiniGame miniGames;
}


[System.Serializable]
public class SpaceData {
	public Space space_obj;
	public bool playerPresent;
	public bool empty = true;
}

[System.Serializable]
public class MiniGameData {
	public Level l;
	public Trap t;
	public int i;
	public MiniGame g;
}

[System.Serializable]
public class TrapData {
	public Level level_id;
	public Trap trap_obj;
	public int space_id;
	public MiniGameType type;
	public MiniGame g;
}


[System.Serializable]
public enum AttackType {
	None,
	Mele,
	Poison,
	Plague,
	Stun
}