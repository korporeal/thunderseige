﻿using UnityEngine;
using Prototype.NetworkLobby;
using System.Collections;
using UnityEngine.Networking;

public class NetworkLobbyHook : LobbyHook {

	public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer) {

		LobbyPlayer lobby = lobbyPlayer.GetComponent<LobbyPlayer>();
		PlayerNetworkMOVE localPlayer = gamePlayer.GetComponent<PlayerNetworkMOVE>();

		localPlayer.playerType = lobby.playerType;
		localPlayer.playerName = lobby.playerName;
	}
}
