﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TurnManager : MonoBehaviour {

	public GameStates currentState = GameStates.None;

	[SerializeField] Text connectionText;
	[SerializeField] Transform[] spawnPoints;
	[SerializeField] Camera sceneCamera;

	void Start() {

		//start TurnEngine

		/*
		 * TurnEngine()
		 * - types (of Turn states):
		 * 	- player
		 * 	- enemy
		 * 	- server
		 * 	- client
		 * 	- init
		 * 	- score 
		 * 	- 
		 * 
		 * 
		 * */

		//wait for a user to invoke a server
		//START or JOIN

		//call for clients

		//call to register hardware

		//



	}

	void Update() {


		//if we're registering the server
		if (currentState == GameStates.RegisterServerTurn) {



		}

	}

	public void addPlayer(Player p) {
		Server.registerPlayer(p);
	}

	public void addLevel(Level l) {
		Server.addLevel(l);
	}



}

[System.Serializable]
public enum GameStates {
	None,
	Startup,
	RegisterServerTurn,
	RegisterClientTurn,
	RegisterMasterTurn,
	RegisterSlavesTurn,
	RegisterPlayersTurn,
	GameBeginTurn,
	PlayerTurn,
	EnemyTurn,
	MiniGameTurn
}