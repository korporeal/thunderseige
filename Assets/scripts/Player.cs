﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class Player : NetworkBehaviour {

	public int space_current;

	public string name;

	public int turnCount = 0;

	[Header("Powerups & Stats")]
	public PlayerType type;

	public PlayerAttributes attributes = new PlayerAttributes();

	public List<Buff> powerups = new List<Buff>(); 

	public List<Shard> shards = new List<Shard>();

	public PortalKey portalKey = null;

	public Canvas ui_stats;


	[Header("Platform UI Pieces")]
	public Text ui_playertype_text;
	public InputField ui_playername_input;
	public Text ui_playername_text;
	public Button button_left;
	public Button button_right;
	public bool ui_mode_chooseModel = true;

	[Header("Scene Objects")]
	public GameObject modelPositionRoot;

	//------ local vars ----------
	private List<PlayerChooseObj> models;

	private Vector3 locON = new Vector3(0,1,0);
	private Vector3 locOFF = new Vector3(0,-100,0);

	private int currentModelIndex = 0;


	[SyncVar]
	private string p_data_playerType;

	[SyncVar]
	private string p_data_playerName;


	//------------- INIT ------------------
	public Player(string name_in, PlayerType type_in, PlayerAttributes attr = default(PlayerAttributes)) {

		name = name_in;
		type = type_in;

		//if it's custom
		if(attr == null) {

			//Make a default character - no special configs
			makeDefaultCharacter(attributes,type);

		} else {

			attributes = attr;

		}//end if/else

	}//end f

	public void Start() {
		models = new List<PlayerChooseObj>();

		if(!isLocalPlayer) {
			ui_mode_chooseModel = false;
		}

		if(!ui_mode_chooseModel) {
			//ui_type_text.gameObject.SetActive(false);
			ui_playername_input.gameObject.SetActive(false);

			button_left.gameObject.SetActive(false);
			button_right.gameObject.SetActive(false);
		} else {
			ui_playername_text.gameObject.SetActive(false);
		}

		addPlayerModels(.1f);

	}

	public override void OnStartLocalPlayer()
	{
		base.OnStartLocalPlayer();

		Vector3 tmpLoc = transform.position;
		tmpLoc.y = 1.25f;
		transform.position = tmpLoc;

		Invoke("moveCameraToSetupLoc", 0.5f);
	}

	public void makeDefaultCharacter(PlayerAttributes p,PlayerType t) {

		attributes.setDefaultAttributes(p, t);

	}//end f


	public void addPlayerModels(float scaleFactor) {


		GameObject berzerker = Instantiate (Resources.Load ("PLAYER_PREFAB_BERZERKER2") as GameObject);
		berzerker.transform.SetParent(modelPositionRoot.transform);
		berzerker.transform.localPosition = new Vector3(0,-100,0);
		berzerker.transform.localEulerAngles = new Vector3(0,0,0);

		PlayerChooseObj tmp1 = new PlayerChooseObj(0, "BERZERKER", berzerker);
		models.Add(tmp1);


		GameObject ranger = Instantiate(Resources.Load("PLAYER_PREFAB_RANGER2") as GameObject);
		ranger.transform.SetParent(modelPositionRoot.transform);
		ranger.transform.localPosition = new Vector3(0,-100,0);
		ranger.transform.localEulerAngles = new Vector3(0,0,0);

		PlayerChooseObj tmp2 = new PlayerChooseObj(1, "RANGER", ranger);
		models.Add(tmp2);


		GameObject mage = Instantiate(Resources.Load("PLAYER_PREFAB_MAGE2") as GameObject);
		mage.transform.SetParent(modelPositionRoot.transform);
		mage.transform.localPosition = new Vector3(0,-100,0);
		mage.transform.localEulerAngles = new Vector3(0,0,0);
		//mage.transform.localScale = Server.v3scaler(mage.transform.localScale, scaleFactor);
		//mage.GetComponent<Player>().modeTurntable();

		PlayerChooseObj tmp3 = new PlayerChooseObj(2, "MAGE", mage);
		models.Add(tmp3);


		GameObject fairy = Instantiate(Resources.Load("PLAYER_PREFAB_FAIRY2") as GameObject);
		fairy.transform.SetParent(modelPositionRoot.transform);
		fairy.transform.localPosition = new Vector3(0,-100,0);
		fairy.transform.localEulerAngles = new Vector3(0,0,0);

		PlayerChooseObj tmp4 = new PlayerChooseObj(3, "FAIRY", fairy);
		models.Add(tmp4);

		showModelByIndex(0);

	}//end f


	//------------- TURN ------------------
	public void turnBegin() {

	}

	public void turn() {
	}

	public void turnEnd() {

	}



	//------------- POWERUPS ------------------
	public bool collectPowerUp(Buff p) { 

		if(powerups.Contains(p)) {
			return false;
		}

		powerups.Add(p);
		return true;
	
	}


	public void playerUsePowerUp(Buff b) {

		b.useBuff(attributes,b);

	}//end f



	//-------------- MOVING -----------------
	public void movePlayer(Vector3 loc) {
		LeanTween.move(gameObject, loc, 2.0f);
	}

	public void modeTurntable() {
		//ui_stats.enabled = false;
	}

	public void indexAdd() {

		//0-4
		if(currentModelIndex + 1 <= models.Count) {
			currentModelIndex++;
		}

		if(currentModelIndex + 1 > models.Count) {
			currentModelIndex = 0;
		}

	}//end f

	public void indexLower() {

		if(currentModelIndex - 1 >= 0) {
			currentModelIndex--;
			return;
		}

		if(currentModelIndex - 1 < 0) {
			currentModelIndex = models.Count-1;
		}

		//Server.o("indexLower() - currentModelIndex: " + currentModelIndex + " --: " + models.Count);

	}//end f

	public string getPlayerTypeByIndex(int idx) {

		foreach(PlayerChooseObj i in models) {

			if(i.idx == idx) {
				return i.type;
			}

		}

		return null;

	}


	//----------------- GUI/VISUAL -----------------------

	public void ui_rightButton() {
		if(!isLocalPlayer) {
			return;
		}
		indexAdd();
		showModelByIndex(currentModelIndex);


	}

	public void ui_leftButton() {
		if(!isLocalPlayer) {
			return;
		}
		indexLower();
		showModelByIndex(currentModelIndex);

	}//end f

	public void showModelByIndex(int p) {

		locON = new Vector3(0,1,0);
		locOFF = new Vector3(0,-100,0);

		foreach(PlayerChooseObj i in models) {

			i.obj.gameObject.transform.localPosition = locOFF;	

			if(p == i.idx) {
				i.obj.gameObject.transform.localPosition = locON;

				ui_playertype_text.text = i.type;
				CmdPlayerType(i.type,i.idx);
			}

		}//end foreach

	}//end f

	public void setPlayerName() {

		string tmp;

		if(isLocalPlayer) {
			tmp = ui_playername_input.text;
			p_data_playerName = tmp;

			//update clients
			CmdPlayerName(p_data_playerName);
		} 

	}


	void moveCameraToSetupLoc() {

		GameObject tmp = Server.getMainCam();

		Vector3 tmp2 = transform.position;

		tmp2.x += 4;
		tmp2.y = 2.5f;
	
		//cam z is left/right

		//cam x is depth

		//----- MATCH Z to spawned player

		//------ SET X to 3

		LeanTween.move(tmp, tmp2, 1.0f);

	}

	//----------------- SERVER COMMANDS -----------------------

	//player type updated - notify server
	[Command]
	public void CmdPlayerType(string t,int idx) {
		p_data_playerType = t;
		RpcPlayerType(t,idx);
	}

	//tell all clients to update player type
	[ClientRpc]
	public void RpcPlayerType(string t,int idx) {

		//only for client copies of me
		if(!isLocalPlayer) {
			if(ui_playertype_text.enabled) {
				ui_playertype_text.text = t;
				showModelByIndex(idx);
			}
		}

	}

	//player name updated - notify server
	[Command]
	public void CmdPlayerName(string n) {
		p_data_playerName = n;
		RpcPlayerName(n);
	}

	//tell all clients to update player name
	[ClientRpc]
	public void RpcPlayerName(string n) {

		//only for client copies of me
		if(!isLocalPlayer) {
			if(ui_playername_text.enabled) {
				ui_playername_text.text = n;
			}
		}
	}


}//end class


[System.Serializable]
public enum PlayerType {
	None,
	Berzerker,
	Ranger,
	Sorcerer,
	Fairy

}

[System.Serializable]
public class PlayerChooseObj {

	public int idx;
	public string type;
	public GameObject obj;

	public PlayerChooseObj(int i,string n,GameObject g) {
		idx = i;
		type = n;
		obj = g;
	}

}


