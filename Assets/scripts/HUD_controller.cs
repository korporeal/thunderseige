﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Networking;

public class HUD_controller : MonoBehaviour {

	public Dropdown player_menu;
	public Dropdown actions_menu;
	public Dictionary<string,System.Action> actions_menu_keypairs = new Dictionary<string,System.Action>();

	public Text dice_display;

	public GameObject mainCam;

	private int dice_value = 0;

	// Use this for initialization
	void Awake () {
		setup_actions();
		Server.init();
		Server.registerMainCam(mainCam);
	}

	
	// Update is called once per frame
	void Update () {
	
	}

	//---------- RENDERING -----------


	void setup_actions() {

		actions_menu.onValueChanged.AddListener(actions_menu_change_handler);
		actions_menu_add("roll",actions_menu_roll_handler);
	}

	void actions_menu_add(string item, System.Action f) {

		actions_menu_keypairs.Add(item,f);

		actions_menu.options.Add(new Dropdown.OptionData(item));

	}

	System.Action actions_menu_getActionById(int tmp) {

		int ii = 0;

		foreach (KeyValuePair<string,System.Action> i in actions_menu_keypairs) {

			if(ii == tmp) {
				return i.Value;
			}

			ii++;
		}

		return null;
	}
		
	/*
	System.Action actions_menu_getFunctionByString(string str) {

		System.Action tmp = null;

		actions_menu_keypairs.TryGetValue(str, out tmp);

		if(tmp != null) {
			return tmp;
		}

		return null;

	}*/

	public void actions_menu_change_handler(int val) {

		Server.o("dropdown int: " + val);

		System.Action tmp = actions_menu_getActionById(val-1);

		//run function
		tmp.Invoke();

	}

	public void actions_menu_roll_handler() {
		
		Server.o("ROLL DICE");

		float rand = Random.Range(0,7);
		dice_value = Mathf.RoundToInt(rand);

		if(dice_value == 0) {
			dice_value++;
		}

		dice_display.text = "Dice Roll: " + dice_value.ToString();

	}


	public void action_spawnPlayerForRegister() {

		//Player tmp = new Player("MY HERO", PlayerType.Berzerker);
		GameObject playerSpawn = Server.spawnPlayer("MY HERO",PlayerType.Berzerker).gameObject;

		Player p = playerSpawn.GetComponent<Player>();

		Level registerLvl = Server.getRegisterLevel();
	
		registerLvl.addPlayer(p);

		//gameObject.GetComponent<NetworkManagerCustom>().spawnPlayer(playerSpawn);

		//NetworkServer.Spawn(playerSpawn);

		//playerSpawn.GetComponent<PlayerNetworkMOVE>().CmdSpawn();

	}

	public void action_spawnPlayerForRegister2() {

		//Player tmp = new Player("MY HERO", PlayerType.Berzerker);
		GameObject playerSpawn = Server.spawnPlayer("MY HERO2",PlayerType.Berzerker).gameObject;

		Player p = playerSpawn.GetComponent<Player>();

		Level registerLvl = Server.getRegisterLevel();

		registerLvl.addPlayer(playerSpawn.GetComponent<Player>(),"space2");

		//gameObject.GetComponent<NetworkManagerCustom>().spawnPlayer(playerSpawn);

		//NetworkServer.Spawn(playerSpawn);

		//playerSpawn.GetComponent<PlayerNetworkMOVE>().CmdSpawn(playerSpawn,registerLvl.spaces[0].transform.localPosition);



	}

}
