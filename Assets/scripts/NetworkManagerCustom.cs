﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkManagerCustom : NetworkManager {

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
	{
		GameObject playerSpawn = (GameObject)Object.Instantiate(Resources.Load("PLAYER_PREFAB")) as GameObject;
		NetworkServer.AddPlayerForConnection(conn, playerSpawn, playerControllerId);
		Server.registerPlayer(playerSpawn.GetComponent<Player>());
	}

}

