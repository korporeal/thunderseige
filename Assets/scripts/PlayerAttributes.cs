﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAttributes {
	public int health = 0;
	public int attack = 0;
	public int speed = 0;
	public int vision = 0;
	public int invisibility = 0;

	public bool stunned = false;
	public bool poisoned = false;
	public bool plagued = false;

	public int resist_poison_chance = 0;
	public int resist_plague_chance = 0;
	public int resist_stun_chance = 0;

	public int inflict_stun_chance = 0;

	public List<Power> powers = new List<Power>();

	public void setDefaultAttributes(PlayerAttributes p, PlayerType pl) {

		switch(pl) {

			case PlayerType.Berzerker:

				PlayerAttributes default_berzerker_attr = new PlayerAttributes();

				default_berzerker_attr.health = 100;
				default_berzerker_attr.attack = 20;
				default_berzerker_attr.speed = 1;
				default_berzerker_attr.vision = 1;
				default_berzerker_attr.invisibility = 0;

				default_berzerker_attr.resist_poison_chance = 80;
				default_berzerker_attr.resist_plague_chance = 80;
				default_berzerker_attr.resist_stun_chance = 50;

				default_berzerker_attr.inflict_stun_chance = 50;

			break;


			case PlayerType.Ranger:

				PlayerAttributes default_ranger_attr = new PlayerAttributes();

				default_ranger_attr.health = 70;
				default_ranger_attr.attack = 10;
				default_ranger_attr.speed = 3;
				default_ranger_attr.vision = 4;
				default_ranger_attr.invisibility = 15;

				default_ranger_attr.resist_poison_chance = 20;
				default_ranger_attr.resist_plague_chance = 20;
				default_ranger_attr.resist_stun_chance = 40;

				default_ranger_attr.inflict_stun_chance = 25;

			break;


			case PlayerType.Sorcerer:

				PlayerAttributes default_sorcerer_attr = new PlayerAttributes();

				default_sorcerer_attr.health = 30;
				default_sorcerer_attr.attack = 0;
				default_sorcerer_attr.speed = 2;
				default_sorcerer_attr.vision = 2;
				default_sorcerer_attr.invisibility = 0;

				default_sorcerer_attr.resist_poison_chance = 100;
				default_sorcerer_attr.resist_plague_chance = 100;
				default_sorcerer_attr.resist_stun_chance = 10;

				Power deal_plague = new Power(PlayerPower.DealPlague, 1000, 1);
				Power deal_poison = new Power(PlayerPower.DealPoison, 1000, 1);

				Power heal_health = new Power(PlayerPower.HealHealth, 20, 1);
				Power heal_plague = new Power(PlayerPower.HealPlague, 100, 1);
				Power heal_poison = new Power(PlayerPower.HealPoison, 100, 1);

				default_sorcerer_attr.powers.Add(deal_plague);
				default_sorcerer_attr.powers.Add(deal_poison);

				default_sorcerer_attr.powers.Add(heal_health);
				default_sorcerer_attr.powers.Add(heal_plague);
				default_sorcerer_attr.powers.Add(heal_poison);



			break;


			case PlayerType.Fairy:

				PlayerAttributes default_fairy_attr = new PlayerAttributes();

				default_fairy_attr.health = 10;
				default_fairy_attr.attack = 0;
				default_fairy_attr.speed = 10;
				default_fairy_attr.vision = 15;
				default_fairy_attr.invisibility = 5;

				default_fairy_attr.resist_poison_chance = 100;
				default_fairy_attr.resist_plague_chance = 100;

				Power tp = new Power(PlayerPower.Teleport, 1000, 3);
				Power speed_double = new Power(PlayerPower.DoubleSpeed, 1000, 3);
				Power stun = new Power(PlayerPower.Stun, 1000, 3);

				default_fairy_attr.powers.Add(tp);
				default_fairy_attr.powers.Add(speed_double);
				default_fairy_attr.powers.Add(stun);

			break;

		}//end switch

	}//end f

}//end class