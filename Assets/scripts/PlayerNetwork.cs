﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerNetwork : NetworkBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	[ClientRpc]
	public void RpcRespawn(Vector3 loc)
	{
		if (isLocalPlayer)
		{
			// move back to zero location
			transform.position = loc;
		}
	}
}
