﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class Level : MonoBehaviour {

	public string name;

	public LevelType type;

	public List<Space> spaces = new List<Space>();

	void Start() {

		//register spaces
		foreach(Transform i in transform) {

			if(i.gameObject.tag == "Space") {

				spaces.Add(i.gameObject.GetComponent<Space>());
				NetworkManager.RegisterStartPosition(i.transform);

			}

		}

		//register level with server
		Server.addLevel(gameObject.GetComponent<Level>());

	}

	Space get_spaceByString(string id) {

		foreach(Space i in spaces) {
			if(i.gameObject.name == id) {
				return i;
			}
		}

		return null;
	}


	public void addPlayer(Player p, string id = "space1") {

		Space sp = get_spaceByString(id);

		p.transform.parent = sp.gameObject.transform;

		movePlayer(p,id);

	}

	public void movePlayer(Player p, string space) {

		Vector3 tmp = get_spaceLocalPosition(space);

		p.GetComponent<PlayerNetworkMOVE>().movePlayer(tmp);
		p.transform.localPosition = tmp;

	}

	public Vector3 get_spaceLocalPosition(string id) {

		foreach(Space i in spaces) {
			if(i.transform.gameObject.name == id) {
				return i.transform.localPosition;
			}
		}

		//wont let me return null....
		return new Vector3(0,0,0);

	}//end f
		
}

[System.Serializable]
public enum LevelType {
	None,
	Register,
	Graveyard,
	BlackForrest,
	Caverns,
	Drawbridge,
	DeadSea
}
