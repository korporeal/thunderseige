﻿using UnityEngine;
using System.Collections;

public class MoveOffOn : MonoBehaviour {

	public GameObject refON;
	public GameObject refOFF;

	private Vector3 locON;
	private Vector3 locOFF;

	// Use this for initialization
	void Start () {
		locON = refON.transform.localPosition;
		locOFF = refOFF.transform.localPosition;
	}
	

	public void moveMeOn(bool turnOn, GameObject me = null) {

		Transform t = transform;

		if(me) { 
			t = me.transform;
		}

		if(turnOn) {
			t.localPosition = locON;
		} else {
			t.localPosition = locOFF;
		}

	}

}
