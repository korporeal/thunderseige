﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using UnityEngine.Networking;


public class ArduinoLink : MonoBehaviour {

	SerialPort stream = null;

	public Player player_red;
	public Enemy enemy_purp;

	public Level level1;

	GameObject[] players;
	GameObject[] game_spaces;

	// Use this for initialization
	void Start () {

		return;

		fakeINIT();

		getPortNames();

		stream = new SerialPort("/dev/tty.usbserial-AI03Y5TP", 9600, Parity.None, 8, StopBits.One);
		stream.ReadTimeout = 200;
		stream.Open();
	
	}

	void fakeINIT() {
		Server.registerArduinoLInk(this);
		Server.addLevel(level1);
		Server.registerPlayer(player_red);
		Server.addEnemy(enemy_purp);
	}

	void getPortNames () {
		
		int p = (int)System.Environment.OSVersion.Platform;
		List<string> serial_ports = new List<string> ();

		// Are we on Unix?
		if (p == 4 || p == 128 || p == 6) {
			string[] ttys = System.IO.Directory.GetFiles ("/dev/", "tty.*");
			foreach (string dev in ttys) {
				if (dev.StartsWith ("/dev/tty.*"))
					serial_ports.Add (dev);
				Debug.Log (System.String.Format (dev));
			}
		}
	}

	void Update() {

		if(stream == null) {
			return;
		}

		if(stream.IsOpen) {

			try {
				string tmp = stream.ReadLine();
				print(tmp);
			}

			catch (System.Exception) {
				print("exception thrown");
			}

		}

	}

	void readData(string arduino_command) {

		string[] splitsky = arduino_command.Split(new string[] {"#"},System.StringSplitOptions.None);

		ArrayList tmp = new ArrayList();

		for(int i = 0; i < splitsky.Length; i++) {
			tmp.Add(splitsky [i]);
		}

		if(tmp[0] == "move") {
			move_player(tmp);
		}

	}


	public void move_player(ArrayList data) {

		/*
		 * 0 - category:move
		 * 1 - id_player
		 * 2 - id_space
		 */

		GameObject playerObj = player_get(data[1].ToString());
		GameObject spaceObj = space_get(data[2].ToString());

		Player playerObjLogic = playerObj.GetComponent<Player>();
		Space spaceObjLogic = spaceObj.GetComponent<Space>();

		LeanTween.move(playerObj, spaceObjLogic.player_loc, 2.0f);

		spaceObjLogic.id_character = playerObj;
		//spaceObjLogic.set_space_material(playerObjLogic.mat);
		

	}



	GameObject player_get(string player_id) {

		foreach(GameObject i in players) {

			if(i.name == player_id) {
				return i;
			}

		}

		return new GameObject();

	}

	GameObject space_get(string space_id) {
		
		foreach(GameObject i in game_spaces) {

			if(i.name == space_id) {
				return i;
			}

		}

		return new GameObject();

	}

	public void staticMode() {

		Destroy(gameObject.GetComponent<PlayerNetworkMOVE>());
		Destroy(gameObject.GetComponent<NetworkProximityChecker>());
		Destroy(gameObject.GetComponent<NetworkTransform>());
		Destroy(gameObject.GetComponent<NetworkIdentity>()); 
	}

}
