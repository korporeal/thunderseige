﻿using UnityEngine;
using System.Collections;

public class Shard : QuestItem {

	public string name;
	public ShardType type;

}

[System.Serializable]
public enum ShardType {
	None,
	Ruby,
	Golden,
	Crystal,
	Sapphire,
	Opal
}
