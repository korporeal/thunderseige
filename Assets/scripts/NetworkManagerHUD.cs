﻿using UnityEngine;
using System.Collections;

public class NetworkManagerHUD : MonoBehaviour {

	public GameObject JoinCreatePanel;
	public GameObject ListGamesPanel;

	public JoinGame joinGameGO;

	// Use this for initialization
	void Start () {
		serverListVisible(false);
		joinGameGO.RefreshRoomList();
	}

	public void serverListVisible(bool isOn) {
		gameObject.GetComponent<MoveOffOn>().moveMeOn(isOn, ListGamesPanel);
		gameObject.GetComponent<MoveOffOn>().moveMeOn(!isOn, JoinCreatePanel);
	}

	public void switchScene(string s) {

		Server.goToScene(s);

	}

}
