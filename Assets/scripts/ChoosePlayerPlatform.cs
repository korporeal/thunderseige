﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Prototype.NetworkLobby {
		
	public class ChoosePlayerPlatform : NetworkLobbyPlayer {

		public bool isLocalPlatorm = false;

		[Header("Platform UI Pieces")]
		public Text ui_type_text;
		public InputField ui_playername_input;
		public Text ui_playername_text;
		public Button button_left;
		public Button button_right;

		[Header("Scene Objects")]
		public GameObject platform;

	
		//------ local vars ----------
		private List<PlayerChooseObj> models;

		private Vector3 locON = new Vector3(0,1,0);
		private Vector3 locOFF = new Vector3(0,-100,0);

		private int currentModelIndex = 0;

		//[SyncVar(hook = "OnMyName")]
		[SyncVar]
		private string p_data_playerType;

		[SyncVar]
		private string p_data_playerName;


		//----------------- INIT -----------------------

		void Start () {
			models = new List<PlayerChooseObj>();

			if(!isLocalPlatorm) {
				//ui_type_text.gameObject.SetActive(false);
				ui_playername_input.gameObject.SetActive(false);

				button_left.gameObject.SetActive(false);
				button_right.gameObject.SetActive(false);
			} else {
				ui_playername_text.gameObject.SetActive(false);
			}
		}


		void addPlayerModels(float scaleFactor) {

			GameObject berzerker = Instantiate (Resources.Load ("PLAYER_PREFAB_BERZERKER") as GameObject);
			berzerker.transform.SetParent(platform.transform);
			berzerker.transform.localPosition = locOFF;
			berzerker.transform.localScale = Server.v3scaler(berzerker.transform.localScale, scaleFactor);
			berzerker.GetComponent<Player>().modeTurntable();

			PlayerChooseObj tmp1 = new PlayerChooseObj(0, "BERZERKER", berzerker);
			models.Add(tmp1);


			GameObject ranger = Instantiate(Resources.Load("PLAYER_PREFAB_RANGER") as GameObject);
			ranger.transform.SetParent(platform.transform);
			ranger.transform.localPosition = locOFF;
			ranger.transform.localScale = Server.v3scaler(ranger.transform.localScale, scaleFactor);
			ranger.GetComponent<Player>().modeTurntable();

			PlayerChooseObj tmp2 = new PlayerChooseObj(1, "RANGER", ranger);
			models.Add(tmp2);


			GameObject mage = Instantiate(Resources.Load("PLAYER_PREFAB_MAGE") as GameObject);
			mage.transform.SetParent(platform.transform);
			mage.transform.localPosition = locOFF;
			mage.transform.localScale = Server.v3scaler(mage.transform.localScale, scaleFactor);
			mage.GetComponent<Player>().modeTurntable();

			PlayerChooseObj tmp3 = new PlayerChooseObj(2, "MAGE", mage);
			models.Add(tmp3);


			GameObject fairy = Instantiate(Resources.Load("PLAYER_PREFAB_FAIRY") as GameObject);
			fairy.transform.SetParent(platform.transform);
			fairy.transform.localPosition = locOFF;
			fairy.transform.localScale = Server.v3scaler(fairy.transform.localScale, scaleFactor);
			fairy.GetComponent<Player>().modeTurntable();

			PlayerChooseObj tmp4 = new PlayerChooseObj(3, "FAIRY", fairy);
			models.Add(tmp4);

			showModelByIndex(0);

		}//end f


		//---------------- OVERRIDE EVENTS -----------------------

		public override void OnClientEnterLobby()
		{
			base.OnClientEnterLobby();

			if (LobbyManager.s_Singleton != null) LobbyManager.s_Singleton.OnPlayersNumberModified(1);

			//PedestalController.
			//LobbyPlayerList._instance.AddPlayer(this);
			//LobbyPlayerList._instance.DisplayDirectServerWarning(isServer && LobbyManager.s_Singleton.matchMaker == null);

			if (isLocalPlayer)
			{
				SetupLocalPlayer();
			}
			else
			{
				SetupOtherPlayer();
			}

			//setup the player data on UI. The value are SyncVar so the player
			//will be created with the right value currently on server
			//OnMyName(playerName);
			//OnMyColor(playerColor);
			//OnMyPlayerType(playerType);
		}

		void SetupOtherPlayer() {

			/*
			nameInput.interactable = false;
			removePlayerButton.interactable = NetworkServer.active;

			ChangeReadyButtonColor(NotReadyColor);

			readyButton.transform.GetChild(0).GetComponent<Text>().text = "...";
			readyButton.interactable = false;
			*/

			OnClientReady(false);

		}

		void SetupLocalPlayer() {

			addPlayerModels(1.4f);

			/*
			nameInput.interactable = true;
			remoteIcone.gameObject.SetActive(false);
			localIcone.gameObject.SetActive(true);

			CheckRemoveButton();

			if (playerColor == Color.white)
				CmdColorChange();

			ChangeReadyButtonColor(JoinColor);

			readyButton.transform.GetChild(0).GetComponent<Text>().text = "JOIN";
			readyButton.interactable = true;

			//have to use child count of player prefab already setup as "this.slot" is not set yet
			if (playerName == "")
				CmdNameChanged("Player" + (LobbyPlayerList._instance.playerListContentTransform.childCount-1));

			//we switch from simple name display to name input
			colorButton.interactable = true;
			nameInput.interactable = true;

			nameInput.onEndEdit.RemoveAllListeners();
			nameInput.onEndEdit.AddListener(OnNameChanged);

			colorButton.onClick.RemoveAllListeners();
			colorButton.onClick.AddListener(OnColorClicked);

			readyButton.onClick.RemoveAllListeners();
			readyButton.onClick.AddListener(OnReadyClicked);
			*/

			//when OnClientEnterLobby is called, the loval PlayerController is not yet created, so we need to redo that here to disable
			//the add button if we reach maxLocalPlayer. We pass 0, as it was already counted on OnClientEnterLobby
			if (LobbyManager.s_Singleton != null) LobbyManager.s_Singleton.OnPlayersNumberModified(0);
		}


		//----------------- DATA -----------------------

		public void indexAdd() {

			//0-4
			if(currentModelIndex + 1 <= models.Count) {
				currentModelIndex++;
			}

			if(currentModelIndex + 1 > models.Count) {
				currentModelIndex = 0;
			}

		}//end f

		public void indexLower() {

			if(currentModelIndex - 1 >= 0) {
				currentModelIndex--;
				return;
			}

			if(currentModelIndex - 1 < 0) {
				currentModelIndex = models.Count-1;
			}

			//Server.o("indexLower() - currentModelIndex: " + currentModelIndex + " --: " + models.Count);

		}//end f

		public string getPlayerTypeByIndex(int idx) {

			foreach(PlayerChooseObj i in models) {

				if(i.idx == idx) {
					return i.type;
				}

			}

			return null;

		}


		//----------------- GUI/VISUAL -----------------------

		public void ui_rightButton() {

			indexAdd();
			showModelByIndex(currentModelIndex);

		}

		public void ui_leftButton() {

			indexLower();
			showModelByIndex(currentModelIndex);

		}//end f

		public void showModelByIndex(int p) {

			foreach(PlayerChooseObj i in models) {

				i.obj.gameObject.transform.localPosition = locOFF;	

				if(p == i.idx) {
					i.obj.gameObject.transform.localPosition = locON;		
					ui_type_text.text = i.type;
				}

			}//end foreach

		}//end f

		public void setPlayerName() {

			string tmp;

			if(isLocalPlayer) {
				tmp = ui_playername_input.text;
			} else {
				tmp = ui_playername_input.text;
			}

			p_data_playerName = tmp;
			CmdPlayerName(p_data_playerName);

		}


			


		//----------------- SERVER COMMANDS -----------------------
		[Command]
		public void CmdPlayerType(string t) {
			p_data_playerType = t;
		}

		[Command]
		public void CmdPlayerName(string n) {
			p_data_playerName = n;
		}
			
	}//end class

}//end namespace

