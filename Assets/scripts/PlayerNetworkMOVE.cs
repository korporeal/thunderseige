﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerNetworkMOVE : NetworkBehaviour {

	public struct PlayerData {

		public string p_name;
		public string p_type;

		public int p_health;
		public int p_attack;
		public int p_speed;
		public int p_vision;
		public int p_invisibility;

		public int p_resist_poison_change;
		public int p_resist_plague_chance;
		public int p_resist_stun_chance;
	
	}

	[SyncVar]
	public string playerType = "none";

	[SyncVar]
	public string playerName = "none";

	private Text ui_name;

	public NetworkHash128 assetID { get; set; }

	void Start() {

		//TODO-fix
		//ui_name = transform.FindChild("p_name").GetComponent<Text>();

		if(!ui_name) {
			Server.o("ERROR - couldn't find p_name UI Text");
		}

	}

	// Update is called once per frame
	void Update () {

		if(!isLocalPlayer) {
			return;
		}

		var x = Input.GetAxis("Horizontal")*0.1f;
		var z = Input.GetAxis("Vertical")*0.1f;

		transform.Translate(x, 0, z);

		Debug.Log("PlayerName: " + playerName + " -- playerType: " + playerType);

	}


	void setPlayerType() {
		
	}


	public void movePlayer(Vector3 localPos) {

		//transform.localPosition = localPos;



	}

	//-----> TO: Server ----->
	[Command]
	public void CmdSpawn(GameObject go, Vector3 pos) {

		//go.transform.localPosition = pos;

		//NetworkServer.Spawn(go);

	}

	// ----> FROM: Server -------->
	[ClientRpc]
	void RpcSpawn(GameObject go) {

		//Rpc can only receive byte, int, float, string, Uint64, structs, Vec3, Quaternion, 
		//NetworkIdentity, NetworkInstanceId, NetworkHash128, GameObject WITH NetworkIdentity component


		//if(NetworkServer.active) return;
		//NetworkServer.SpawnWithClientAuthority(go, connectionToClient);
	}

}

/*
[SyncVar(hook="OnHasPlayerDefUpdated")] 
struct PlayerDef { 
	string m_Name;
	string m_Type;
	Color m_Color; 

	public PlayerDef( string name, string type, Color color ) { 
		m_Name = name; 
		m_Type = type;
		m_Color = color; 
	} 
}*/