﻿using UnityEngine;
using System.Collections;

public class Power : MonoBehaviour {

	public PlayerPower type;
	public int strength;
	public int cooldown;

	public Power(PlayerPower power_in, int strength_in, int cooldown_in) {
		type = power_in;
		strength = strength_in;
		cooldown = cooldown_in;
	}
}

[System.Serializable]
public enum PlayerPower {
	None,
	HealHealth,
	HealPlague,
	HealPoison,
	DealPlague,
	DealPoison,
	DoubleSpeed,
	Teleport,
	Stun 
}